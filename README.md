# Woishop E-commerce

This is an e-commerce code repository, 
This is a project that I created and designed. For e-commerce web development.

It will provide APIs for the frontend to do the following things:

* Create and manage products
* Create and manage authentication and authorization
* Create and manage carts
* Create and manage categorys 
* Crate and manage images
* Record all balance changes to each of the carts

The programming language we will use to develop the service is Golang, but the course is not just about coding in Go.

1. First we will design the application, design the database, and its data structure
2. in the second part we will establish a connection to the database, then create a restfull api. here I use the gorm ORM with mysql db, and to handle the api I use GIN-GONIC.
3. In the third part I will create logic for the business itself, such as how to create a cart for each user and how products can be selected etc
4. In the fourth part I try to make pagination, sort, search to make it easier for clients to find their products or carts

## Woishop services

The service that we’re going to build is a e-commerce. It will provide APIs for the frontend to do following things:

1. PRODUCT MANAGEMENT - Add Product, Delete Product, Get all products, get products by categorys
, get product by search, sort, pagination
2. record all changes to the cart user, such as quantity, delivery, products etc
3. CART MANAGEMENT - Add cart, delete cart, get all cart, get cart by id, get cart by search, sort, and pagination
4. CATEGORYS - Get all categorys
5. IMAGES - Get all images
6. USER - Get user, update user
7. JWT AUTHENTICATION FOR API'S
- GET TOKEN
- AUHTORIZE ON EVERY API CALL
8. Entity->Repository->Service->Controllers

## Clean Architecture
Independent of Frameworks. The architecture does not depend on the existence of some library of feature laden software. This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.
Testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.
Independent of UI. The UI can change easily, without changing the rest of the system. A Web UI could be replaced with a console UI, for example, without changing the business rules.
Independent of Database. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else. Your business rules are not bound to the database.
Independent of any external agency. In fact your business rules simply don’t know anything at all about the outside world.

![architecture schema appliaction!](clean architecture.png)

### The Dependency Rule
* Entities
* Repositorys
* Services
* Controllers

## DB schema

![Database schema appliaction!](dbschema.png)

## Setup Local Development

### Install Tools

* *[GOLANG](https://go.dev/)*
* *[GORM](https://gorm.io/)*
* *[GIN-GONIC](https://gin-gonic.com/)*

### How to run

Move to directory :
* cd $GOPATH/golang/src/product_directory

Clone into YOUR $GOPATH/golang/src/product_directory :
* git@gitlab.com:aldisaputra17/product_directory.git

Move to project :
* cd product_directory

Run the script :
* ./woishop.exe or go run main.go

Listen port :
* Site runs at Authentication
    > http://127.0.0.1:8080/api/v1/auth/register (post)
    > http://127.0.0.1:8080/api/v1/auth/login (post)
* Site runs at Users
    > http://127.0.0.1:8080/api/v1/users (get)
    > http://127.0.0.1:8080/api/v1/user/:id (put)
* Site runs at Products
    > http://127.0.0.1:8080/api/v1/products (get)
    > http://127.0.0.1:8080/api/v1/products/search (get with search, sort, and pagination)
    > http://127.0.0.1:8080/api/v1/product (post add prod)
    > http://127.0.0.1:8080/api/v1/product/:id (get)
    > http://127.0.0.1:8080/api/v1/products/category_id (get product by categorys)
* Site runs at Categorys
    > http://127.0.0.1:8080/api/v1/categorys (get)
* Site runs at Images 
    > http://127.0.0.1:8080/api/v1/images (get)
* Site runs at Carts
    > http://127.0.0.1:8080/api/v1/cart (post add cart)
    > http://127.0.0.1:8080/api/v1/carts (get)
    > http://127.0.0.1:8080/api/v1/carts/search (get with search, sort, pagination carts)
    > http://127.0.0.1:8080/api/v1/images (get)
    > http://127.0.0.1:8080/api/v1/cart/:id (get by id)
    > http://127.0.0.1:8080/api/v1/cart/:id (put cart)
    > http://127.0.0.1:8080/api/v1/cart/:id (delete)
* Site runs at Pengirimans
    > http://127.0.0.1:8080/api/v1/pengirimans (get)
    > http://127.0.0.1:8080/api/v1/pengiriman (post create pengiriman)


