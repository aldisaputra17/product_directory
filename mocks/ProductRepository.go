package mocks

import (
	"context"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/stretchr/testify/mock"
)

type ProductRepository struct {
	mock.Mock
}

func (_m *ProductRepository) Delete(ctx context.Context, id uint64) (bool, error) {
	ret := _m.Called(ctx, id)

	var r0 bool
	if rf, ok := ret.Get(0).(func(context.Context, uint64) bool); ok {
		r0 = rf(ctx, id)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint64) error); ok {
		r1 = rf(ctx, id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

func (_m *ProductRepository) FindAll(ctx context.Context) ([]entity.Product, error) {
	ret := _m.Called(ctx)

	var r0 []entity.Product
	if rf, ok := ret.Get(0).(func(context.Context) []entity.Product); ok {
		r0 = rf(ctx)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]entity.Product)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context) error); ok {
		r1 = rf(ctx)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

func (_m *ProductRepository) FindByQuery(ctx context.Context, name string) ([]*entity.Product, error) {
	ret := _m.Called(ctx, name)

	var r0 []*entity.Product
	if rf, ok := ret.Get(0).(func(context.Context, string) []*entity.Product); ok {
		r0 = rf(ctx, name)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*entity.Product)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, name)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

func (_m *ProductRepository) Create(ctx context.Context, prod *entity.Product) (*entity.Product, error) {
	ret := _m.Called(ctx, prod)

	var r0 *entity.Product
	if rf, ok := ret.Get(0).(func(context.Context, *entity.Product) *entity.Product); ok {
		r0 = rf(ctx, prod)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*entity.Product)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, *entity.Product) error); ok {
		r1 = rf(ctx, prod)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

func (_m *ProductRepository) FindProductByCategory(ctx context.Context, categoryID uint64) ([]*entity.Product, error) {
	ret := _m.Called(ctx, categoryID)

	var r0 []*entity.Product
	if rf, ok := ret.Get(0).(func(context.Context, uint64) []*entity.Product); ok {
		r0 = rf(ctx, categoryID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*entity.Product)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint64) error); ok {
		r1 = rf(ctx, categoryID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

func (_m *ProductRepository) FindByID(ctx context.Context, Id uint64) (*entity.Product, error) {
	ret := _m.Called(ctx, Id)

	var r0 *entity.Product
	if rf, ok := ret.Get(0).(func(context.Context, uint64) *entity.Product); ok {
		r0 = rf(ctx, Id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*entity.Product)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, uint64) error); ok {
		r1 = rf(ctx, Id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
