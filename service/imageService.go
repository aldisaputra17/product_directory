package service

import (
	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/repository"
)

type ImageService interface {
	FindImage() []entity.Image
}

type imageService struct {
	imageRepository repository.ImageRepository
}

func NewImageService(imgRepo repository.ImageRepository) ImageService {
	return &imageService{
		imageRepository: imgRepo,
	}
}

func (service *imageService) FindImage() []entity.Image {
	return service.imageRepository.FindImage()
}
