package service

import (
	"context"
	"fmt"
	"time"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"

	"github.com/aldisaputra17/woishop/repository"
	"github.com/aldisaputra17/woishop/request"
	"github.com/gin-gonic/gin"
)

type ProductService interface {
	FindAll(ctx context.Context) ([]entity.Product, error)
	FindByID(ctx context.Context, prodID uint64) (*entity.Product, error)
	FindProductByCategory(ctx context.Context, CategoryID uint64) ([]*entity.Product, error)
	// FindWithPagination(ctx context.Context, prod *entity.Product, paginat *entity.Pagination) (*[]entity.Product, int64, error)
	Store(ctx context.Context, reqProd *request.ProductRequest) (*entity.Product, error)
	PaginantionProduct(ctx *gin.Context, paginat *entity.Pagination) (helpers.Response, error)
}

type productService struct {
	productRepository repository.ProductRepository
	contexTimeout     time.Duration
}

func NewProductService(productRepo repository.ProductRepository, time time.Duration) ProductService {
	return &productService{
		productRepository: productRepo,
		contexTimeout:     time,
	}
}

func (service *productService) FindAll(ctx context.Context) ([]entity.Product, error) {
	ctx, cancel := context.WithTimeout(ctx, service.contexTimeout)
	defer cancel()

	listProd, err := service.productRepository.FindAll(ctx)
	if err != nil {
		return nil, err
	}
	return listProd, nil
}

func (service *productService) FindByID(ctx context.Context, prodID uint64) (*entity.Product, error) {
	ctx, cancel := context.WithTimeout(ctx, service.contexTimeout)
	defer cancel()

	res, err := service.productRepository.FindByID(ctx, prodID)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (service *productService) FindProductByCategory(ctx context.Context, categoryID uint64) ([]*entity.Product, error) {
	ctx, cancel := context.WithTimeout(ctx, service.contexTimeout)
	defer cancel()
	res, err := service.productRepository.FindProductByCategory(ctx, categoryID)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (service *productService) Store(ctx context.Context, reqProd *request.ProductRequest) (*entity.Product, error) {
	prodStore := &entity.Product{
		Name:       reqProd.Name,
		Price:      reqProd.Price,
		Stock:      reqProd.Stock,
		UserID:     reqProd.UserID,
		CategoryID: reqProd.CategoryID,
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
	}

	ctx, cancel := context.WithTimeout(ctx, service.contexTimeout)
	defer cancel()

	res, err := service.productRepository.Create(ctx, prodStore)
	if err != nil {
		return nil, err
	}
	return res, nil

}

func (service *productService) PaginantionProduct(ctx *gin.Context, paginat *entity.Pagination) (helpers.Response, error) {
	operationResult, totalPages := service.productRepository.PaginationProduct(paginat)

	if operationResult.Error != nil {
		return helpers.Response{Status: true, Message: operationResult.Error.Error()}, nil
	}

	var data = operationResult.Result.(*entity.Pagination)

	urlPath := ctx.Request.URL.Path

	searchQueryParams := ""

	for _, search := range paginat.Searchs {
		searchQueryParams += fmt.Sprintf("&%s.%s=%s", search.Column, search.Action, search.Query)
	}

	data.FirstPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, 0, paginat.Sort) + searchQueryParams
	data.LastPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, totalPages, paginat.Sort) + searchQueryParams

	if data.Page > 0 {
		// set previous page pagination response
		data.PreviousPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, data.Page-1, paginat.Sort) + searchQueryParams
	}

	if data.Page < totalPages {
		// set next page pagination response
		data.NextPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, data.Page+1, paginat.Sort) + searchQueryParams
	}

	if data.Page > totalPages {
		// reset previous page
		data.PreviousPage = ""
	}
	return helpers.BuildResponse(true, "Ok", data), nil
}
