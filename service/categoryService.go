package service

import (
	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/repository"
)

type CategoryService interface {
	FindCategory() []entity.Category
}

type categoryService struct {
	categoryRepository repository.CategoryRepository
}

func NewCategoryService(categoryRepo repository.CategoryRepository) CategoryService {
	return &categoryService{
		categoryRepository: categoryRepo,
	}
}

func (service *categoryService) FindCategory() []entity.Category {
	return service.categoryRepository.FindCategory()
}
