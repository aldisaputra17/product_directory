package service

import (
	"log"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/repository"
	"github.com/aldisaputra17/woishop/request"
	"github.com/mashingan/smapping"
)

type UserService interface {
	UpdateUser(user request.RequestUpdateUser) entity.User
	GetUser(userID string) entity.User
}

type userService struct {
	userRepository repository.UserRepository
}

func NewUserService(userRepo repository.UserRepository) UserService {
	return &userService{
		userRepository: userRepo,
	}
}

func (service *userService) UpdateUser(user request.RequestUpdateUser) entity.User {
	userToUpdate := entity.User{}
	err := smapping.FillStruct(&userToUpdate, smapping.MapFields(&user))
	if err != nil {
		log.Fatalf("Failed map %v:", err)
	}
	updatedUser := service.userRepository.UpdateUser(userToUpdate)
	return updatedUser
}

func (service *userService) GetUser(userID string) entity.User {
	return service.userRepository.GetUser(userID)
}
