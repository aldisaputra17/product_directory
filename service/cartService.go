package service

import (
	"context"
	"fmt"
	"time"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/repository"
	"github.com/aldisaputra17/woishop/request"
	"github.com/gin-gonic/gin"
)

type CartService interface {
	FindAll(ctx context.Context, userID uint64) ([]*entity.Cart, error)
	FindByID(ctx context.Context, cartID uint64) (*entity.Cart, error)
	Store(ctx context.Context, cartReq *request.RequestCreateCart) (*entity.Cart, error)
	RefreshCart(ctx context.Context, userID uint64) error
	UpdateCart(ctx context.Context, cartReq *request.RequestUpdateCart) (*entity.Cart, error)
	Delete(ctx context.Context, cart *entity.Cart) error
	GetTotalCartValue(cart []*entity.Cart) int
	IsAllowedToEdit(ctx context.Context, userID string, cartID uint64) bool
	PaginantionCart(ctx *gin.Context, paginat *entity.Pagination) (helpers.Response, error)
}

type cartService struct {
	cartRepository repository.CartRepository
	contextTimeout time.Duration
}

func NewCartService(cartRepo repository.CartRepository, time time.Duration) CartService {
	return &cartService{
		cartRepository: cartRepo,
		contextTimeout: time,
	}
}

func (service *cartService) FindAll(ctx context.Context, userID uint64) ([]*entity.Cart, error) {
	ctx, cancel := context.WithTimeout(ctx, service.contextTimeout)
	defer cancel()
	updateErr := service.RefreshCart(ctx, userID)
	if updateErr != nil {
		return nil, updateErr
	}
	res, err := service.cartRepository.FindAll(ctx, userID)

	if err != nil {
		return nil, err
	}

	return res, nil
}

func (service *cartService) FindByID(ctx context.Context, cartID uint64) (*entity.Cart, error) {
	ctx, cancel := context.WithTimeout(ctx, service.contextTimeout)
	defer cancel()
	res, err := service.cartRepository.FindById(ctx, cartID)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (service *cartService) RefreshCart(ctx context.Context, userID uint64) error {
	ctx, cancel := context.WithTimeout(ctx, service.contextTimeout)
	defer cancel()

	mCart, err := service.cartRepository.CovertDetailCartMap(ctx, userID)
	if err != nil {
		return err
	}

	for _, v := range mCart {
		if v.TotalHarga == 0 {
			v.TotalHarga = v.Price * v.Quantity
			_, err := service.cartRepository.Update(ctx, v)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (service *cartService) UpdateCart(ctx context.Context, cartReq *request.RequestUpdateCart) (*entity.Cart, error) {
	cartUpdate := &entity.Cart{
		ID:           cartReq.ID,
		Catatan:      cartReq.Catatan,
		Quantity:     cartReq.Quantity,
		PengirimanID: cartReq.PengirimanID,
		UserID:       cartReq.UserID,
	}
	fmt.Println("uc:", cartUpdate)
	ctx, cancel := context.WithTimeout(ctx, service.contextTimeout)
	defer cancel()

	res, err := service.cartRepository.UpdateDetailCart(ctx, cartUpdate)
	if err != nil {
		return nil, err
	}

	fmt.Println("id:", res)
	return res, nil
}

func (service *cartService) Delete(ctx context.Context, cart *entity.Cart) error {
	ctx, cancel := context.WithTimeout(ctx, service.contextTimeout)
	defer cancel()

	return service.cartRepository.Delete(ctx, cart)
}

func (service *cartService) GetTotalCartValue(cart []*entity.Cart) int {
	total := 0
	for i := 0; i < len(cart); i++ {
		total = total + (cart[i].Quantity)*cart[i].Price
	}
	return total
}

func (service *cartService) Store(ctx context.Context, cartReq *request.RequestCreateCart) (*entity.Cart, error) {
	cartCreate := &entity.Cart{
		Catatan:      cartReq.Catatan,
		Quantity:     cartReq.Quantity,
		PengirimanID: cartReq.PengirimanID,
		ProductID:    cartReq.ProductID,
		UserID:       cartReq.UserID,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	}
	fmt.Println("uc:", cartCreate)
	ctx, cancel := context.WithTimeout(ctx, service.contextTimeout)
	defer cancel()

	res, err := service.cartRepository.Create(ctx, cartCreate)
	if err != nil {
		return nil, err
	}

	fmt.Println("id:", res)
	return res, nil
}

func (service *cartService) IsAllowedToEdit(ctx context.Context, userID string, cartID uint64) bool {
	c, err := service.cartRepository.FindById(ctx, cartID)
	if err != nil {
		return false
	}
	id := fmt.Sprintf("%v", c.UserID)
	return userID == id
}

func (service *cartService) PaginantionCart(ctx *gin.Context, paginat *entity.Pagination) (helpers.Response, error) {
	operationResult, totalPages := service.cartRepository.PaginationCart(paginat)

	if operationResult.Error != nil {
		return helpers.Response{Status: true, Message: operationResult.Error.Error()}, nil
	}

	var data = operationResult.Result.(*entity.Pagination)

	urlPath := ctx.Request.URL.Path

	searchQueryParams := ""

	for _, search := range paginat.Searchs {
		searchQueryParams += fmt.Sprintf("&%s.%s=%s", search.Column, search.Action, search.Query)
	}

	data.FirstPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, 0, paginat.Sort) + searchQueryParams
	data.LastPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, totalPages, paginat.Sort) + searchQueryParams

	if data.Page > 0 {
		// set previous page pagination response
		data.PreviousPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, data.Page-1, paginat.Sort) + searchQueryParams
	}

	if data.Page < totalPages {
		// set next page pagination response
		data.NextPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, paginat.Limit, data.Page+1, paginat.Sort) + searchQueryParams
	}

	if data.Page > totalPages {
		// reset previous page
		data.PreviousPage = ""
	}
	return helpers.BuildResponse(true, "Ok", data), nil
}
