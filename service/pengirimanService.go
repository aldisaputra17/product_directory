package service

import (
	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/repository"
	"github.com/aldisaputra17/woishop/request"
)

type JasaPengirimanService interface {
	Create(jp request.RequestPengiriman) (entity.Pengiriman, error)
	FindAll() []entity.Pengiriman
	FindById(jpID uint64) entity.Pengiriman
}

type jasaPengirimanService struct {
	repository repository.JasaPengirimanRepository
}

func NewJasaPengirimanService(repository repository.JasaPengirimanRepository) JasaPengirimanService {
	return &jasaPengirimanService{
		repository: repository,
	}
}

func (service *jasaPengirimanService) Create(jp request.RequestPengiriman) (entity.Pengiriman, error) {
	createJP := entity.Pengiriman{
		Name: jp.Name,
	}

	res := service.repository.Create(createJP)
	return res, nil
}

func (service *jasaPengirimanService) FindAll() []entity.Pengiriman {
	return service.repository.FindAll()
}

func (service *jasaPengirimanService) FindById(jpID uint64) entity.Pengiriman {
	return service.repository.FindById(jpID)
}
