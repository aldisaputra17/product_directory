package database

import (
	"database/sql"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/aldisaputra17/woishop/entity"
	// _ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type dbconn struct {
	DB  *gorm.DB
	SDB *sql.DB
}

var dbinst *dbconn
var once sync.Once

func GetDBInstance() *dbconn {
	once.Do(func() {
		d := DBConnection()
		dbinst = &dbconn{DB: d}
	})
	return dbinst
}

func DBConnection() *gorm.DB {
	errEnv := godotenv.Load()
	if errEnv != nil {
		panic("Failed to load env file")
	}
	dbUser := os.Getenv("DB_USER")
	dbPass := os.Getenv("DB_PASS")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")

	dsn := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8&parseTime=True&loc=Local", dbUser, dbPass, dbHost, dbName)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		NowFunc: func() time.Time {
			ti, _ := time.LoadLocation("Asia/Jakarta")
			return time.Now().In(ti)
		},
	})
	if err != nil {
		panic("Failed to create a connection to database")
	}
	db.AutoMigrate(&entity.Product{}, &entity.Category{}, &entity.Image{}, &entity.User{}, &entity.Cart{}, &entity.Pengiriman{}, &entity.Promotion{})
	return db
}

func CloseDatabaseConnection(db *gorm.DB) {
	dbSQL, err := db.DB()
	if err != nil {
		panic("Failed to close connection from database")
	}
	dbSQL.Close()
}
