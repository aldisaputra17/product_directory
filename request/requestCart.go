package request

type RequestCreateCart struct {
	Catatan      string `json:"catatan" form:"catatan" binding:"required"`
	Quantity     int    `json:"quantity" form:"quantity" binding:"required"`
	PengirimanID uint64 `json:"pengiriman_id" form:"pengiriman_id" binding:"required"`
	UserID       uint64 `json:"-"  form:"user_id,omitempty"`
	ProductID    uint64 `json:"product_id" form:"product_id" binding:"required"`
}

type RequestUpdateCart struct {
	ID           uint64 `json:"id" form:"id" binding:"required"`
	Catatan      string `json:"catatan" form:"catatan" binding:"required"`
	Quantity     int    `json:"quantity" form:"quantity" binding:"required"`
	PengirimanID uint64 `json:"pengiriman_id" form:"pengiriman_id" binding:"required"`
	UserID       uint64 `json:"-" form:"user_id,omitempty"`
}
