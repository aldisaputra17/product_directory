package request

type ProductRequest struct {
	Name       string `json:"name" form:"name" binding:"required"`
	Price      int    `json:"price" form:"price" binding:"required"`
	Stock      int    `json:"stock" form:"stock" binding:"required"`
	UserID     uint64 `json:"user_id,omitempty" form:"user_id,omitempty"`
	CategoryID uint64 `json:"category_id,omitempty" form:"category_id,omitempty"`
}
