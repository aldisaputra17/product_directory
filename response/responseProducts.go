package response

import "github.com/aldisaputra17/woishop/entity"

type ResponseProductsByCategory struct {
	ID         uint64          `json:"id"`
	Name       string          `json:"name"`
	Price      uint64          `json:"price"`
	CategoryID uint64          `json:"-"`
	UserID     uint64          `json:"-"`
	Images     *[]entity.Image `json:"images,omitempty"`
	User       entity.User     `json:"-"`
}
