package main

import (
	"fmt"
	"time"

	"github.com/aldisaputra17/woishop/controllers"
	"github.com/aldisaputra17/woishop/database"
	"github.com/aldisaputra17/woishop/docs" // Swagger generated files
	"github.com/aldisaputra17/woishop/middleware"
	"github.com/aldisaputra17/woishop/repository"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
	"gorm.io/gorm"
)

var (
	timeOutContext     time.Duration                        = 10 * time.Second
	db                 *gorm.DB                             = database.DBConnection()
	productRepository  repository.ProductRepository         = repository.NewProductRepository(db)
	userRepository     repository.UserRepository            = repository.NewUserRepository(db)
	imageRepository    repository.ImageRepository           = repository.NewImageRepository(db)
	categoryRepository repository.CategoryRepository        = repository.NewCategoryRepository(db)
	cartRepository     repository.CartRepository            = repository.NewCartRepository(db)
	jpRepository       repository.JasaPengirimanRepository  = repository.NewJasaPengirimanRepository(db)
	userService        service.UserService                  = service.NewUserService(userRepository)
	authService        service.AuthService                  = service.NewAuthService(userRepository)
	jwtService         service.JWTService                   = service.NewJWTService()
	productService     service.ProductService               = service.NewProductService(productRepository, timeOutContext)
	imageService       service.ImageService                 = service.NewImageService(imageRepository)
	categoryService    service.CategoryService              = service.NewCategoryService(categoryRepository)
	cartService        service.CartService                  = service.NewCartService(cartRepository, timeOutContext)
	jpService          service.JasaPengirimanService        = service.NewJasaPengirimanService(jpRepository)
	userController     controllers.UserController           = controllers.NewUserController(userService, jwtService)
	authController     controllers.AuthController           = controllers.NewAuthController(authService, jwtService)
	productController  controllers.ProductController        = controllers.NewProductController(productService, jwtService)
	imageController    controllers.ImageController          = controllers.NewImageController(imageService, jwtService)
	categoryController controllers.CategoryController       = controllers.NewCategoryController(categoryService, jwtService)
	cartController     controllers.CartController           = controllers.NewCartController(cartService, jwtService)
	jpController       controllers.JasaPengirimanController = controllers.NewJasaPengirimanController(jpService, jwtService)
)

// @securityDefinitions.apiKey  bearerAuth
// @in header
// @name Authorization

func main() {

	// Swagger 2.0 Meta Information
	docs.SwaggerInfo.Title = "Swagger Woishop API"
	docs.SwaggerInfo.Description = "This is a sample Woishop API."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Schemes = []string{"http"}

	fmt.Println("Start Server")
	defer database.CloseDatabaseConnection(db)
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	// v1 := r.Group("/api/v1")

	apiRoutes := r.Group(docs.SwaggerInfo.BasePath)

	authRoute := apiRoutes.Group("/auth")
	{
		authRoute.POST("/login", authController.Login)
		authRoute.POST("/register", authController.Register)
	}

	userRoutes := apiRoutes.Group("/", middleware.AuthorizeJWT(jwtService))

	{
		userRoutes.GET("users", userController.GetUser)
		userRoutes.PUT("user/:id", userController.UpdateUser)
	}

	productsRoute := apiRoutes.Group("/", middleware.AuthorizeJWT(jwtService))
	{
		productsRoute.GET("products", productController.FindAll)
		productsRoute.GET("products/search", productController.PaginationProduct)
		productsRoute.POST("product", productController.Store)
		productsRoute.GET("product/:id", productController.FindByID)
		productsRoute.GET("products/:category_id", productController.FindProductByCategory)
	}
	categoryRoute := apiRoutes.Group("/", middleware.AuthorizeJWT(jwtService))
	{
		categoryRoute.GET("categorys", categoryController.FindCategory)

	}
	imageRoute := apiRoutes.Group("/", middleware.AuthorizeJWT(jwtService))
	{
		imageRoute.GET("images", imageController.FindImage)
	}
	cartRoute := apiRoutes.Group("/", middleware.AuthorizeJWT(jwtService))
	{
		cartRoute.POST("cart", cartController.Create)
		cartRoute.GET("carts", cartController.FindCart)
		cartRoute.GET("carts/search", cartController.PaginationCart)
		cartRoute.GET("cart/:id", cartController.FindById)
		cartRoute.PUT("cart/:id", cartController.Update)
		cartRoute.DELETE("cart/:id", cartController.Delete)
	}
	jpRoute := apiRoutes.Group("/", middleware.AuthorizeJWT(jwtService))
	{
		jpRoute.POST("pengiriman", jpController.Create)
		jpRoute.GET("pengirimans", jpController.FindAll)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	// Elastic Beanstalk forwards requests to port 8080
	r.Run()
}
