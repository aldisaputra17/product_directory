package dataobj

type ImageObj struct {
	Name      string `json:"title" form:"name" binding:"required"`
	ProductID uint64 `json:"category_id,omitempty" form:"category_id,omitempty"`
}
