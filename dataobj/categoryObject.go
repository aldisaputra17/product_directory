package dataobj

type CategoryObj struct {
	Name string `json:"title" form:"name" binding:"required"`
}
