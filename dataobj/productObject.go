package dataobj

type ProductObj struct {
	Name       string  `json:"title" form:"name" binding:"required"`
	Price      float64 `json:"price" form:"price" binding:"required"`
	CategoryID uint64  `json:"category_id,omitempty" form:"category_id,omitempty"`
}
