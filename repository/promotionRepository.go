package repository

import (
	"context"

	"github.com/aldisaputra17/woishop/entity"
	"gorm.io/gorm"
)

type PromotionRepository interface {
	Find(ctx context.Context) ([]*entity.Promotion, error)
	// Create(ctx context.Context, prom *entity.Promotion) (int64, error)
	// FetchPromotionwithQuery(ctx context.Context, query string) ([]*entity.Promotion, error)
}

type promotionConnection struct {
	connection *gorm.DB
}

func NewPromotionRepository(dbConn *gorm.DB) PromotionRepository {
	return &promotionConnection{
		connection: dbConn,
	}
}

// func (db *promotionConnection) Create(ctx context.Context, prom *entity.Promotion) (int64, error) {
// 	prR := NewProductRepository(db.connection)
// 	prodItems, err := prR.FindAllByID(ctx, prom.ProductID)
// 	if err != nil {
// 		return 0, err
// 	}

// 	if len(prodItems) <= 0 {
// 		slenErr := fmt.Errorf("Source Item doesnt exists")
// 		return 0, slenErr
// 	}

// 	// prodQuery = "SELECT id, name, price, stock FROM products where id = '" + strconv.Itoa(a.Dprodid) + "'"

// 	dprodItems, derr := prR.FindByQuery(ctx, prom.UserID)
// 	if derr != nil {
// 		return 0, derr
// 	}

// 	if len(dprodItems) <= 0 {
// 		dlenErr := fmt.Errorf("DEstination Item doesnt exists")
// 		return 0, dlenErr
// 	}

// 	promoItems, err := db.FindProdIDandDprodID(ctx, prom.ProductID, prom.UserID)
// 	fmt.Println("modely:", len(prodItems), prom.ProductID, prom.UserID)
// 	if err != nil {
// 		return 0, err
// 	}

// 	if len(promoItems) > 0 {
// 		fmt.Println("Item already exists in promotion. Please delete and add again ")
// 		return 0, errors.New("Item already exists in promotion. Please delete and add again ")
// 	}
// 	query := `INSERT INTO promotions ( product_id , sminqty , dprodid , dminqty , disctype , discount,priority ) VALUES ( ? , ? , ? , ? , ? , ? , ?)`

// 	stmt, err := db.connection.ConnPool.PrepareContext(ctx, query)
// 	if err != nil {
// 		fmt.Println(err)
// 		return 0, err
// 	}

// 	res, err := stmt.ExecContext(ctx, prom.ProductID, prom.Sminqty, prom.UserID, prom.Dminqty, prom.Disctype, prom.Discount, prom.Priority)

// 	if err != nil {
// 		return 0, nil
// 	}

// 	return res.LastInsertId()
// 	// prodQuery = "SELECT id, name, price, stock FROM products where id = '" + strconv.Itoa(prom.Dprodid) + "'"
// }

func (db *promotionConnection) FindProdIDandDprodID(ctx context.Context, prodID uint64, dpID uint64) ([]*entity.Promotion, error) {
	var prom []*entity.Promotion
	res := db.connection.WithContext(ctx).Where("product_id = ? and dpprod_id = ?", prodID, dpID).Find(&prom)
	if res.Error != nil {
		return nil, res.Error
	}
	return prom, nil
}

func (db *promotionConnection) Find(ctx context.Context) ([]*entity.Promotion, error) {
	var prom []*entity.Promotion
	res := db.connection.Find(&prom)
	if res.Error != nil {
		return nil, res.Error
	}
	return prom, nil
}

// func (db *promotionConnection) FetchPromotionwithQuery(ctx context.Context, query string) ([]*entity.Promotion, error) {
// 	return db.Create(ctx, query)
// }
