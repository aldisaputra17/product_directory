package repository

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strings"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	// "gorm.io/gorm/logger"
)

type CartRepository interface {
	FindAll(ctx context.Context, userID uint64) ([]*entity.Cart, error)
	CovertDetailCartMap(ctx context.Context, user uint64) (map[uint64](*entity.Cart), error)
	Update(ctx context.Context, cart *entity.Cart) (*entity.Cart, error)
	UpdateDetailCart(ctx context.Context, cart *entity.Cart) (*entity.Cart, error)
	Delete(ctx context.Context, cart *entity.Cart) error
	FindById(ctx context.Context, id uint64) (*entity.Cart, error)
	Create(ctx context.Context, cart *entity.Cart) (*entity.Cart, error)
	FetchPromotionDetail(ctx context.Context, userID uint64) ([]*entity.Promotion, error)
	GetCartByProdIDandUserID(ctx context.Context, userID uint64, prodID uint64) ([]*entity.Cart, error)
	PaginationCart(pagination *entity.Pagination) (helpers.PaginationResult, int)
}

type cartConnection struct {
	connection *gorm.DB
}

func NewCartRepository(db *gorm.DB) CartRepository {
	return &cartConnection{
		connection: db,
	}
}

func (db *cartConnection) Update(ctx context.Context, cart *entity.Cart) (*entity.Cart, error) {
	query := `UPDATE carts set total_harga = ? WHERE id = ?`

	stmt, err := db.connection.ConnPool.PrepareContext(ctx, query)
	if err != nil {
		return nil, err
	}

	res, err := stmt.ExecContext(ctx, cart.TotalHarga, cart.ID)
	if err != nil {
		return nil, err
	}

	rowAffected, err := res.RowsAffected()
	if err != nil {
		return nil, err
	}

	if rowAffected != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", rowAffected)
		logrus.Error(err)
		return nil, err
	}

	return cart, nil
}

func (db *cartConnection) CovertDetailCartMap(ctx context.Context, userID uint64) (map[uint64](*entity.Cart), error) {
	// query := "SELECT id, catatan, total_harga, total_pembayaran, quantity, price, product_id, user_id, pengiriman_id FROM carts WHERE user_id = ?"
	cartList, err := db.FindAll(ctx, userID)
	if err != nil {
		fmt.Println("Error", err)
		return nil, err
	}
	fmt.Println("Cartlist:", cartList)
	var mapCart = map[uint64]*entity.Cart{}
	for i := 0; i < len(cartList); i++ {
		mapCart[cartList[i].ProductID] = cartList[i]
	}
	return mapCart, nil
}

func (db *cartConnection) Delete(ctx context.Context, cart *entity.Cart) error {
	res := db.connection.Delete(&cart)
	if res.Error != nil {
		return nil
	}
	return nil
}

// func (db *cartConnection) FindCartUser(ctx context.Context, userID uint64) ([]*entity.Cart, error) {
// 	query := `SELECT id, catatan, total_harga, total_pembayaran, quantity, price,
// 	product_id, user_id, pengiriman_id
// 						FROM carts where user_id = ?`
// 	return db.fetch(ctx, query, userID)
// }

func (db *cartConnection) FindAll(ctx context.Context, userID uint64) ([]*entity.Cart, error) {
	var cart []*entity.Cart
	res := db.connection.WithContext(ctx).Preload("Products.Category").Joins("Pengirimans").Joins("User").Where("user_id = ?", userID).Find(&cart)
	if res.Error != nil {
		return nil, res.Error
	}
	return cart, nil
}

func (db *cartConnection) FindById(ctx context.Context, id uint64) (*entity.Cart, error) {
	var cart *entity.Cart
	res := db.connection.Where("id = ?", id).Preload("Products").Preload("Products.Images").Preload("Products.Category").Preload("Pengirimans").Find(&cart).Find(&cart)
	if res.Error != nil {
		return nil, res.Error
	}
	return cart, nil
}

func (db *cartConnection) GetCartByProdIDandUserID(ctx context.Context, userID uint64, prodID uint64) ([]*entity.Cart, error) {
	var carts []*entity.Cart
	res := db.connection.WithContext(ctx).Where("user_id = ? and product_id = ?", userID, prodID).Find(&carts)
	if res.Error != nil {
		return nil, res.Error
	}

	return carts, nil
}

func (db *cartConnection) Create(ctx context.Context, cart *entity.Cart) (*entity.Cart, error) {
	prR := NewProductRepository(db.connection)
	// entity.Cart{Price: prod.Price}
	// prodQuery := "SELECT id, name, price, stock, category_id, user_id FROM products where id = '" + strconv.Itoa(int(cart.ProductID)) + "' "
	prodItems, err := prR.FindAllByID(ctx, cart.ProductID)
	if err != nil {
		return nil, err
	}

	if len(prodItems) <= 0 {
		lenErr := fmt.Errorf("Product doesnt exists")
		return nil, lenErr
	}

	cart.TotalHarga = prodItems[0].Price * cart.Quantity
	cart.Name = prodItems[0].Name
	cart.Price = prodItems[0].Price
	if cart.Quantity > prodItems[0].Stock {
		stockErr := fmt.Errorf("Stock too high")
		fmt.Println("Stock too much requested")
		return nil, stockErr
	}
	fmt.Println("model:", cart.UserID, cart.ProductID)
	// cartQuery := `SELECT id, catatan, total_harga, total_pembayaran, quantity, price,
	// product_id, user_id, pengiriman_id
	// 					FROM carts where user_id = ? and product_id = ?`
	cartItems, err := db.GetCartByProdIDandUserID(ctx, cart.UserID, cart.ProductID)
	fmt.Println("modely:", len(cartItems), cart.UserID, cart.ProductID, err)
	if err != nil {
		return nil, err
	}

	if len(cartItems) > 0 {
		fmt.Println("Item already exists in cart. Please delete and add again ")
		return nil, errors.New("Item already exists in cart. Please delete and add again ")
	}

	// query := `INSERT INTO carts ( catatan , total_harga  , total_pembayaran , quantity , price ,
	// 	pengiriman_id, product_id, user_id)
	// VALUES ( ? , ? , ? , ? , ? , ? )`

	res := db.connection.WithContext(ctx).Create(&cart)
	db.connection.Preload("Products").Preload("Products.Images").Preload("Products.Category").Preload("Pengirimans").Find(&cart)
	if res.Error != nil {
		fmt.Println(err)
		return nil, res.Error
	}

	// res, err := stmt.ConnPool.ExecContext(ctx, cart.Catatan, cart.TotalHarga, cart.TotalPembayaran, cart.Quantity, cart.Price, cart.PengirimanID, cart.ProductID, cart.UserID)

	// if err != nil {
	// 	return 0, err
	// }

	return cart, nil
}

func (db *cartConnection) UpdateDetailCart(ctx context.Context, cart *entity.Cart) (*entity.Cart, error) {
	res := db.connection.WithContext(ctx).Model(&cart).Updates(entity.Cart{Catatan: cart.Catatan,
		PengirimanID: cart.PengirimanID, Quantity: cart.Quantity})
	db.connection.Preload("Products").Preload("Products.Images").Preload("Products.Category").Preload("Pengirimans").Find(&cart)
	if res.Error != nil {
		return nil, res.Error
	}
	cart.TotalHarga = cart.Price * cart.Quantity
	return cart, nil
}

func (db *cartConnection) PaginationCart(pagination *entity.Pagination) (helpers.PaginationResult, int) {
	var cart []entity.Cart

	var (
		totalRows  int64
		totalPages int
		fromRow    int
		toRow      int
	)

	offset := pagination.Page * pagination.Limit

	find := db.connection.Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)

	searchs := pagination.Searchs

	for _, value := range searchs {
		column := value.Column
		action := value.Action
		query := value.Query

		switch action {
		case "equals":
			whereQuery := fmt.Sprintf("%s = ?", column)
			find = find.Where(whereQuery, query)
		case "contains":
			whereQuery := fmt.Sprintf("%s LIKE ?", column)
			find = find.Where(whereQuery, "%"+query+"%")
		case "in":
			whereQuery := fmt.Sprintf("%s IN (?)", column)
			queryArray := strings.Split(query, ",")
			find = find.Where(whereQuery, queryArray)
		}
	}

	find = find.Preload("Products").Preload("Products.Images").Preload("Products.Category").Preload("Pengirimans").Find(&cart)

	errFind := find.Error

	if errFind != nil {
		return helpers.PaginationResult{Error: errFind}, totalPages
	}

	pagination.Rows = cart

	errCount := db.connection.Model(&entity.Cart{}).Count(&totalRows).Error

	if errCount != nil {
		return helpers.PaginationResult{Error: errCount}, totalPages
	}

	pagination.TotalRows = int(totalRows)

	totalPages = int(math.Ceil(float64(totalRows)/float64(pagination.Limit))) - 1

	if pagination.Page == 0 {
		fromRow = 1
		toRow = pagination.Limit
	} else {
		if pagination.Page <= totalPages {
			fromRow = pagination.Page*pagination.Limit + 1
			toRow = (pagination.Page + 1) * pagination.Limit
		}
	}

	if toRow > int(totalRows) {
		toRow = int(totalRows)
	}

	pagination.FromRow = fromRow
	pagination.ToRow = toRow

	return helpers.PaginationResult{Result: pagination}, totalPages
}

func (db *cartConnection) FetchPromotionDetail(ctx context.Context, userID uint64) ([]*entity.Promotion, error) {
	var promo []*entity.Promotion
	res := db.connection.WithContext(ctx).Where("user_id = ?", userID).Find(&promo)
	if res.Error != nil {
		return nil, res.Error
	}
	return promo, nil
}
