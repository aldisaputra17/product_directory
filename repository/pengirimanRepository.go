package repository

import (
	"github.com/aldisaputra17/woishop/entity"
	"gorm.io/gorm"
)

type JasaPengirimanRepository interface {
	Create(jp entity.Pengiriman) entity.Pengiriman
	FindAll() []entity.Pengiriman
	FindById(jpID uint64) entity.Pengiriman
}

type jasaPengirimanConnection struct {
	connection *gorm.DB
}

func NewJasaPengirimanRepository(db *gorm.DB) JasaPengirimanRepository {
	return &jasaPengirimanConnection{
		connection: db,
	}
}

func (db *jasaPengirimanConnection) Create(jp entity.Pengiriman) entity.Pengiriman {
	db.connection.Create(&jp)
	db.connection.Preload("carts")
	return jp
}

func (db *jasaPengirimanConnection) FindAll() []entity.Pengiriman {
	var jp []entity.Pengiriman
	db.connection.Find(&jp)
	return jp
}

func (db *jasaPengirimanConnection) FindById(jpID uint64) entity.Pengiriman {
	var jp entity.Pengiriman
	db.connection.First(&jp, jpID)
	return jp
}
