package repository

import (
	"context"
	"fmt"
	"math"
	"strings"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"gorm.io/gorm"
)

type ProductRepository interface {
	FindAll(ctx context.Context) ([]entity.Product, error)
	FindByID(ctx context.Context, prodID uint64) (*entity.Product, error)
	Create(ctx context.Context, prod *entity.Product) (*entity.Product, error)
	Delete(ctx context.Context, id uint64) (bool, error)
	FindProductByCategory(ctx context.Context, categoryID uint64) ([]*entity.Product, error)
	FindAllByID(ctx context.Context, prodID uint64) ([]*entity.Product, error)
	PaginationProduct(pagination *entity.Pagination) (helpers.PaginationResult, int)
}

type productConnection struct {
	connection *gorm.DB
}

func NewProductRepository(dbConnect *gorm.DB) ProductRepository {
	return &productConnection{
		connection: dbConnect,
	}
}

func (db *productConnection) Create(ctx context.Context, prod *entity.Product) (*entity.Product, error) {
	res := db.connection.WithContext(ctx).Create(prod)
	db.connection.Preload("Category").Preload("Images").Find(&prod)
	if res.Error != nil {
		return nil, res.Error
	}
	return prod, nil
}

func (db *productConnection) FindByID(ctx context.Context, prodID uint64) (*entity.Product, error) {
	var product *entity.Product
	res := db.connection.WithContext(ctx).Where("id = ?", prodID).Preload("Category").Preload("Images").Find(&product)
	if res.Error != nil {
		return nil, res.Error
	}
	return product, nil
}

func (db *productConnection) FindAllByID(ctx context.Context, prodID uint64) ([]*entity.Product, error) {
	var product []*entity.Product
	res := db.connection.WithContext(ctx).Preload("Category").Preload("Images").Find(&product, prodID)
	if res.Error != nil {
		return nil, res.Error
	}
	return product, nil
}

func (db *productConnection) FindProductByCategory(ctx context.Context, categoryID uint64) ([]*entity.Product, error) {
	var product []*entity.Product
	res := db.connection.WithContext(ctx).Where("category_id = ?", categoryID).Preload("Category").Preload("Images").Find(&product)
	if res.Error != nil {
		return nil, res.Error
	}
	return product, nil
}

func (db *productConnection) Delete(ctx context.Context, id uint64) (bool, error) {
	var prod *entity.Product
	res := db.connection.Where("id = ?", id).Delete(&prod)
	if res.Error != nil {
		return false, res.Error
	}
	return true, nil
}

func (db *productConnection) FindAll(ctx context.Context) ([]entity.Product, error) {
	var products []entity.Product
	tx := db.connection.WithContext(ctx).Preload("Category").Preload("Images").
		Find(&products)
	if tx.Error != nil {
		return nil, tx.Error
	}
	return products, nil
}

func (db *productConnection) PaginationProduct(pagination *entity.Pagination) (helpers.PaginationResult, int) {
	var prod []entity.Product

	var (
		totalRows  int64
		totalPages int
		fromRow    int
		toRow      int
	)

	offset := pagination.Page * pagination.Limit

	find := db.connection.Limit(pagination.Limit).Offset(offset).Order(pagination.Sort)

	searchs := pagination.Searchs

	for _, value := range searchs {
		column := value.Column
		action := value.Action
		query := value.Query

		switch action {
		case "equals":
			whereQuery := fmt.Sprintf("%s = ?", column)
			find = find.Where(whereQuery, query)
		case "contains":
			whereQuery := fmt.Sprintf("%s LIKE ?", column)
			find = find.Where(whereQuery, "%"+query+"%")
		case "in":
			whereQuery := fmt.Sprintf("%s IN (?)", column)
			queryArray := strings.Split(query, ",")
			find = find.Where(whereQuery, queryArray)
		}
	}

	find = find.Preload("Category").Preload("Images").Find(&prod)

	errFind := find.Error

	if errFind != nil {
		return helpers.PaginationResult{Error: errFind}, totalPages
	}

	pagination.Rows = prod

	errCount := db.connection.Model(&entity.Product{}).Count(&totalRows).Error

	if errCount != nil {
		return helpers.PaginationResult{Error: errCount}, totalPages
	}

	pagination.TotalRows = int(totalRows)

	totalPages = int(math.Ceil(float64(totalRows)/float64(pagination.Limit))) - 1

	if pagination.Page == 0 {
		fromRow = 1
		toRow = pagination.Limit
	} else {
		if pagination.Page <= totalPages {
			fromRow = pagination.Page*pagination.Limit + 1
			toRow = (pagination.Page + 1) * pagination.Limit
		}
	}

	if toRow > int(totalRows) {
		toRow = int(totalRows)
	}

	pagination.FromRow = fromRow
	pagination.ToRow = toRow

	return helpers.PaginationResult{Result: pagination}, totalPages
}
