package repository

import (
	"github.com/aldisaputra17/woishop/entity"
	"gorm.io/gorm"
)

type CategoryRepository interface {
	FindCategory() []entity.Category
}

type categoryConnection struct {
	connection *gorm.DB
}

func NewCategoryRepository(categoryConn *gorm.DB) CategoryRepository {
	return &categoryConnection{
		connection: categoryConn,
	}
}

func (db *categoryConnection) FindCategory() []entity.Category {
	var categorys []entity.Category
	db.connection.Find(&categorys)
	return categorys
}

