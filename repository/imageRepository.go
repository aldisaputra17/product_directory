package repository

import (
	"github.com/aldisaputra17/woishop/entity"
	"gorm.io/gorm"
)

type ImageRepository interface {
	FindImage() []entity.Image
}

type imageConnection struct {
	connection *gorm.DB
}

func NewImageRepository(imageConn *gorm.DB) ImageRepository {
	return &imageConnection{
		connection: imageConn,
	}
}

func (db *imageConnection) FindImage() []entity.Image {
	var images []entity.Image
	db.connection.Preload("Products").Preload("Products.Category").Find(&images)
	return images
}
