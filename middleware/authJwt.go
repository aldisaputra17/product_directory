package middleware

import (
	"log"
	"net/http"

	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/service"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func AuthorizeJWT(jwtService service.JWTService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			response := helpers.BuildErrorResponse("Failed To Process Response", "Not Token Found!", nil)
			c.AbortWithStatusJSON(http.StatusBadRequest, response)
			return
		}
		token, err := jwtService.ValidateToken(authHeader)
		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			log.Println("Claim[user_id]: ", claims["user_id"])
			log.Println("Claim[issuer] :", claims["issuer"])
		} else {
			log.Println(err)
			response := helpers.BuildErrorResponse("Token not Valid!", err.Error(), nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
		}

	}
}

// func IsAdmin(ctx *gin.Context) {
// 	if ctx.GetHeader("Role") != "admin" {
// 		response := helpers.BuildErrorResponse("Not Authorized", "Fail, role is admin not user", nil)
// 		ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
// 		return
// 	}
// }

// func IsUser(ctx *gin.Context) {
// 	if ctx.GetHeader("Role") != "user" {
// 		response := helpers.BuildErrorResponse("Not Authorized", "Fail, role is admin not user", nil)
// 		ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
// 		return
// 	}
// }
