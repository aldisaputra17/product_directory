package entity

type Promotion struct {
	ID        int    `gorm:"AUTO_INCREMENT" form:"id" json:"id"`
	ProductID uint64 `gorm:"not null" form:"product_id" json:"product_id"`
	Sminqty   int    `gorm:"not null" form:"sminqty" json:"sminqty"`
	UserID    uint64 `gorm:"not null" form:"user_id" json:"user_id"`
	Dminqty   int    `gorm:"not null" form:"dminqty" json:"dminqty"`
	Disctype  string `gorm:"not null" form:"disctype" json:"disctype"`
	Discount  int    `gorm:"not null" form:"discount" json:"discount"`
	Priority  int    `gorm:"not null" form:"priority" json:"priority"`
}
