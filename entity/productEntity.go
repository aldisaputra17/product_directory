package entity

import "time"

type Product struct {
	ID         uint64    `gorm:"primary_key:auto_increment" json:"id"`
	Name       string    `gorm:"type:varchar(255)" json:"name"`
	Price      int       `json:"price"`
	Stock      int       `json:"stock"`
	CreatedAt  time.Time `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt  time.Time `gorm:"autoCreateTime" json:"updated_at"`
	CategoryID uint64    `gorm:"not null" json:"-"`
	UserID     uint64    `gorm:"not null" json:"-"`
	Images     *[]Image  `json:"images,omitempty"`
	Category   Category  `gorm:"foreignkey:CategoryID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"category"`
	User       User      `gorm:"foreignkey:UserID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"-"`
}
