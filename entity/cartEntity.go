package entity

import "time"

type Cart struct {
	ID           uint64     `gorm:"primary_key:auto_increment" json:"id"`
	Catatan      string     `gorm:"type:text" json:"catatan"`
	Name         string     `gorm:"type:varchar(255)" json:"name"`
	TotalHarga   int        `json:"total_harga"`
	Quantity     int        `gorm:"not null" json:"quantity"`
	Price        int        `gorm:"not null" json:"price"`
	CreatedAt    time.Time  `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt    time.Time  `gorm:"autoCreateTime" json:"updated_at"`
	UserID       uint64     `gorm:"not null" json:"user_id"`
	User         User       `gorm:"foreignkey:UserID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"-"`
	ProductID    uint64     `gorm:"not null" json:"-"`
	Products     Product    `gorm:"foreignkey:ProductID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"products"`
	PengirimanID uint64     `gorm:"not null" json:"-"`
	Pengirimans  Pengiriman `gorm:"foreignkey:PengirimanID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"jasa_pengiriman"`
}
