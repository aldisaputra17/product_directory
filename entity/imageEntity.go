package entity

type Image struct {
	ID        uint64  `gorm:"primary_key:auto_increment" json:"id"`
	Name      string  `gorm:"type:varchar(255)" json:"name"`
	ProductID uint64  `gorm:"not null" json:"-"`
	Products  Product `gorm:"foreignkey:ProductID;constraint:onUpdate:CASCADE,onDelete:CASCADE" json:"-"`
}
