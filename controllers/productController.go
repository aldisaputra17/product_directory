package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/request"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
)

type ProductController interface {
	FindAll(ctx *gin.Context)
	FindByID(ctx *gin.Context)
	FindProductByCategory(ctx *gin.Context)
	Store(ctx *gin.Context)
	PaginationProduct(ctx *gin.Context)
}

type productController struct {
	productService service.ProductService
	jwtService     service.JWTService
}

func NewProductController(productServ service.ProductService, jwtServ service.JWTService) ProductController {
	return &productController{
		productService: productServ,
		jwtService:     jwtServ,
	}
}

// Get Products godoc
// @Security bearerAuth
// @Summary List existing product
// @Description Get all the existing product
// @Tags Product
// @Accept  json
// @Produce  json
// @Success 200 {array} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /products [get]
func (c *productController) FindAll(ctx *gin.Context) {
	listProd, err := c.productService.FindAll(ctx)
	fmt.Println("list:", listProd)
	if err != nil {
		res := helpers.BuildErrorResponse("Fail fetch list products", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	res := helpers.BuildResponse(true, "Ok", listProd)
	ctx.JSON(http.StatusOK, res)
}

// Get Products Search, Lazy load pagination godoc
// @Security bearerAuth
// @Summary List existing product by search,sort and pagination
// @Description Get all the existing product by search, sort and pagination
// @Tags Product
// @Accept  json
// @Produce  json
// @param page query string false "product search by page" Format(name)
// @param limit query string false "product search by limit" Format(name)
// @param sort query string false "product search by sort" Format(name)
// @param constains query string false "product search by name.constains" Format(name)
// @param in query string false "product search by name.in" Format(name)
// @param equal query string false "product search by name.equal" Format(name)
// @Success 200 {array} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /products/search [get]
func (c *productController) PaginationProduct(ctx *gin.Context) {
	code := http.StatusOK
	pagination := helpers.GeneratePaginationRequest(ctx)

	response, err := c.productService.PaginantionProduct(ctx, pagination)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed pagination products", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	if !response.Status {
		code = http.StatusBadRequest
	}

	res := helpers.BuildResponse(true, "Ok", response)
	ctx.AbortWithStatusJSON(code, res)
}

// GetProductByID godoc
// @Security bearerAuth
// @Summary Get Product By Id
// @Description Get product by id
// @Tags Product
// @Accept  json
// @Produce  json
// @Param  id path uint64 true "Product ID"
// @Success 200 {object} helpers.Response
// @Failure 404 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /product/{id} [get]
func (c *productController) FindByID(ctx *gin.Context) {
	prodID, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		res := helpers.BuildErrorResponse("No param id was found", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusNotFound, res)
		return
	}
	helpers.BuildErrorResponse("Data not found", "No data with given id", helpers.EmptyObj{})

	response, err := c.productService.FindByID(ctx, prodID)
	if (response == &entity.Product{}) {
		res := helpers.BuildErrorResponse("Fail fetch list products", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	} else {
		res := helpers.BuildResponse(true, "Ok", response)
		ctx.AbortWithStatusJSON(http.StatusOK, res)
	}

}

// GetProductByCategoryID godoc
// @Security bearerAuth
// @Summary Get Product By CategoryID
// @Description Get product by category id
// @Tags Product
// @Accept  json
// @Produce  json
// @Param  category_id path uint64 true "Category ID"
// @Success 200 {array} helpers.Response
// @Failure 404 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 404 {object} helpers.Response
// @Router /products/{category_id} [get]
func (c *productController) FindProductByCategory(ctx *gin.Context) {
	category_id, err := strconv.ParseUint(ctx.Param("category_id"), 0, 0)
	if err != nil {
		res := helpers.BuildErrorResponse("No param category_id was found", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusNotFound, res)
		return
	}
	helpers.BuildErrorResponse("Data not found", "No data with given id", helpers.EmptyObj{})

	if err != nil {
		res := helpers.BuildErrorResponse("Failed Get Data", "No data Product", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
	}

	resp, err := c.productService.FindProductByCategory(ctx, category_id)

	if category_id < 1 {
		res := helpers.BuildErrorResponse("Category Id not found", "No data with given category_id", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusNotFound, res)
	} else {
		res := helpers.BuildResponse(true, "Ok", resp)
		ctx.AbortWithStatusJSON(http.StatusOK, res)
	}
}

// Store Product godoc
// @Security bearerAuth
// @Summary Store Product
// @Description Store Product API
// @Tags Product
// @Accept  json
// @Produce  json
// @Param product body request.ProductRequest true "Store product"
// @Success 200 {object} helpers.Response
// @Failure 422 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /product [post]
func (c *productController) Store(ctx *gin.Context) {
	var product request.ProductRequest
	err := ctx.ShouldBind(&product)
	if err != nil {
		res := helpers.BuildErrorResponse("failed create add product", err.Error(), helpers.EmptyObj{})
		ctx.JSON(http.StatusUnprocessableEntity, res)
		return
	} else {
		authHeader := ctx.GetHeader("Authorization")
		userID := c.getUserIDByToken(authHeader)
		convertedUserID, err := strconv.ParseUint(userID, 10, 64)
		if err == nil {
			product.UserID = convertedUserID
		}
		fmt.Println("Product:", product)
		prod, err := c.productService.Store(ctx, &product)
		if err != nil {
			res := helpers.BuildErrorResponse("product is not valid", err.Error(), helpers.EmptyObj{})
			ctx.JSON(http.StatusBadRequest, res)
			return
		}
		res := helpers.BuildResponse(true, "Success create product", prod)
		ctx.JSON(http.StatusOK, res)
	}

}

func (c *productController) getUserIDByToken(token string) string {
	Token, err := c.jwtService.ValidateToken(token)
	if err != nil {
		panic(err.Error())
	}
	claims := Token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	return id
}
