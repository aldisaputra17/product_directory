package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/request"
	"github.com/aldisaputra17/woishop/service"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type UserController interface {
	UpdateUser(context *gin.Context)
	GetUser(context *gin.Context)
}

type userController struct {
	userService service.UserService
	jwtService  service.JWTService
}

func NewUserController(userService service.UserService, jwtService service.JWTService) UserController {
	return &userController{
		userService: userService,
		jwtService:  jwtService,
	}
}

// Update User godoc
// @Security bearerAuth
// @Summary Update Cart
// @Description Update Cart API
// @Tags User
// @Accept  json
// @Produce  json
// @Param cart body request.RequestUpdateUser true "Update user"
// @Success 200 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /user/{id} [put]
func (c *userController) UpdateUser(context *gin.Context) {
	var userReqUpdate request.RequestUpdateUser
	errObj := context.ShouldBind(&userReqUpdate)
	if errObj != nil {
		res := helpers.BuildErrorResponse("Failed to process request", errObj.Error(), helpers.EmptyObj{})
		context.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	authHeader := context.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	id, err := strconv.ParseUint(fmt.Sprintf("%v", claims["user_id"]), 10, 64)
	if err != nil {
		panic(err.Error())
	}
	userReqUpdate.ID = id
	u := c.userService.UpdateUser(userReqUpdate)
	res := helpers.BuildResponse(true, "OK!", u)
	context.JSON(http.StatusOK, res)
}

// Get User godoc
// @Security bearerAuth
// @Summary List existing cart
// @Description Get all the existing cart
// @Tags User
// @Accept  json
// @Produce  json
// @Success 200 {array} helpers.ResponseCart
// @Router /users [get]
func (c *userController) GetUser(context *gin.Context) {
	authHeader := context.GetHeader("Authorization")
	token, err := c.jwtService.ValidateToken(authHeader)
	if err != nil {
		panic(err.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	user := c.userService.GetUser(id)
	res := helpers.BuildResponse(true, "OK", user)
	context.JSON(http.StatusOK, res)

}
