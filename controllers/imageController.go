package controllers

import (
	"net/http"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
)

type ImageController interface {
	FindImage(ctx *gin.Context)
}

type imageController struct {
	imageService service.ImageService
	jwtService   service.JWTService
}

func NewImageController(srvImg service.ImageService, jwtServ service.JWTService) ImageController {
	return &imageController{
		imageService: srvImg,
		jwtService:   jwtServ,
	}
}

// Get Images godoc
// @Security bearerAuth
// @Summary List existing image
// @Description Get all the existing image
// @Tags Image
// @Accept  json
// @Produce  json
// @Success 200 {array} helpers.Response
// @Router /images [get]
func (c *imageController) FindImage(ctx *gin.Context) {
	var images []entity.Image = c.imageService.FindImage()
	res := helpers.BuildResponse(true, "Ok", images)
	ctx.JSON(http.StatusOK, res)
}
