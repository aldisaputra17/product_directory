package controllers

import (
	"net/http"
	"strconv"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/request"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
)

type AuthController interface {
	Register(ctx *gin.Context)
	Login(ctx *gin.Context)
}

type authController struct {
	authService service.AuthService
	jwtService  service.JWTService
}

func NewAuthController(authService service.AuthService, jwtService service.JWTService) AuthController {
	return &authController{
		authService: authService,
		jwtService:  jwtService,
	}
}

//Paths Information

// Authenticate godoc
// @Summary Provides a JSON Web Token
// @Description Authenticates a user and provides a JWT to Authorize API calls
// @Tags Authenticate
// @ID Authentication
// @Consume application/x-www-form-urlencoded
// @Produce json
// @Param user body request.RequestLogin true "Login"
// @Success 200 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 401 {object} helpers.Response
// @Router /auth/login [post]
func (c *authController) Login(ctx *gin.Context) {
	var reqLogin request.RequestLogin
	errObj := ctx.ShouldBind(&reqLogin)
	if errObj != nil {
		response := helpers.BuildErrorResponse("Failed to process request", errObj.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	authResult := c.authService.VerifyCredential(reqLogin.Email, reqLogin.Password)
	if v, ok := authResult.(entity.User); ok {
		conv := strconv.FormatUint(v.ID, 10)
		generatedToken := c.jwtService.GenerateToken(conv)
		v.Token = generatedToken
		response := helpers.BuildResponse(true, "OK!", v)
		ctx.JSON(http.StatusOK, response)
		return
	}
	response := helpers.BuildErrorResponse("Please check again your credential", "Invalid Credential", helpers.EmptyObj{})
	ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
}

// Register User
// Register godoc
// @Summary Register User
// @Description Register User Endpoint
// @Tags Register
// @Accept  json
// @Produce  json
// @Param user body request.RequestRegister true "Register"
// @Success 201 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 409 {object} helpers.Response
// @Router /auth/register [post]
func (c *authController) Register(ctx *gin.Context) {
	var reqRegister request.RequestRegister
	errObj := ctx.ShouldBind(&reqRegister)
	if errObj != nil {
		response := helpers.BuildErrorResponse("Failed to process request", errObj.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	if !c.authService.IsDuplicateEmail(reqRegister.Email) {
		response := helpers.BuildErrorResponse("Failed to process request", "Duplicate email", helpers.EmptyObj{})
		ctx.JSON(http.StatusConflict, response)
	} else {
		createdUser := c.authService.CreateUser(reqRegister)
		conv := strconv.FormatUint(createdUser.ID, 10)
		token := c.jwtService.GenerateToken(conv)
		createdUser.Token = token
		response := helpers.BuildResponse(true, "OK!", createdUser)
		ctx.JSON(http.StatusCreated, response)
	}
}
