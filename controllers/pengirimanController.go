package controllers

import (
	"net/http"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/request"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
)

type JasaPengirimanController interface {
	Create(ctx *gin.Context)
	FindAll(ctx *gin.Context)
}

type jasaPengirimanController struct {
	jpService  service.JasaPengirimanService
	jwtService service.JWTService
}

func NewJasaPengirimanController(jpService service.JasaPengirimanService, jwt service.JWTService) JasaPengirimanController {
	return &jasaPengirimanController{
		jpService:  jpService,
		jwtService: jwt,
	}
}

// Create Pengiriman godoc
// @Security bearerAuth
// @Summary Create Pengiriman
// @Description Create Pengiriman API
// @Tags Pengiriman
// @Accept  json
// @Produce  json
// @Param product body request.RequestPengiriman true "Create pengiriman"
// @Success 201 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /pengiriman [post]
func (c *jasaPengirimanController) Create(ctx *gin.Context) {
	var reqJP request.RequestPengiriman
	err := ctx.ShouldBind(&reqJP)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed to process request", err.Error(), helpers.EmptyObj{})
		ctx.JSON(http.StatusBadRequest, res)
	}
	result, err := c.jpService.Create(reqJP)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed to created cart", err.Error(), helpers.EmptyObj{})
		ctx.JSON(http.StatusBadRequest, res)
	}
	response := helpers.BuildResponse(true, "OK", result)
	ctx.JSON(http.StatusCreated, response)

}

// Get Pengirimans godoc
// @Security bearerAuth
// @Summary List existing pengiriman
// @Description Get all the existing pengiriman
// @Tags Pengiriman
// @Accept  json
// @Produce  json
// @Success 200 {array} helpers.Response
// @Router /pengirimans [get]
func (c *jasaPengirimanController) FindAll(ctx *gin.Context) {
	var jp []entity.Pengiriman = c.jpService.FindAll()
	res := helpers.BuildResponse(true, "Ok", jp)
	ctx.JSON(http.StatusOK, res)
}
