package controllers

import (
	"net/http"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
)

type CategoryController interface {
	FindCategory(ctx *gin.Context)
}

type categoryController struct {
	categoryService service.CategoryService
	jwtService      service.JWTService
}

func NewCategoryController(cateService service.CategoryService, jwtServ service.JWTService) CategoryController {
	return &categoryController{
		categoryService: cateService,
		jwtService:      jwtServ,
	}
}

// Get Categorys godoc
// @Security bearerAuth
// @Summary List existing category
// @Description Get all the existing category
// @Tags Category
// @Accept  json
// @Produce  json
// @Success 200 {array} helpers.Response
// @Failure 404 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /categorys [get]
func (c *categoryController) FindCategory(ctx *gin.Context) {
	var categorys []entity.Category = c.categoryService.FindCategory()
	res := helpers.BuildResponse(true, "OK", categorys)
	ctx.JSON(http.StatusOK, res)
}
