package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"

	"github.com/aldisaputra17/woishop/entity"
	"github.com/aldisaputra17/woishop/helpers"
	"github.com/aldisaputra17/woishop/request"
	"github.com/aldisaputra17/woishop/service"
	"github.com/gin-gonic/gin"
)

type CartController interface {
	FindCart(ctx *gin.Context)
	FindById(ctx *gin.Context)
	Create(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
	PaginationCart(ctx *gin.Context)
}

type cartController struct {
	cartService service.CartService
	jwtService  service.JWTService
}

func NewCartController(cartService service.CartService, jwtService service.JWTService) CartController {
	return &cartController{
		cartService: cartService,
		jwtService:  jwtService,
	}
}

// Get Cart godoc
// @Security bearerAuth
// @Summary List existing cart
// @Description Get all the existing cart
// @Tags Cart
// @Accept  json
// @Produce  json
// @Success 200 {array} helpers.ResponseCart
// @Failure 404 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /carts [get]
func (c *cartController) FindCart(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if errToken != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	parsID, err := strconv.ParseUint(userID, 10, 64)
	if err != nil {
		res := helpers.BuildErrorResponse("No param cart_id was found", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusNotFound, res)
		return
	}
	listCart, err := c.cartService.FindAll(ctx, parsID)
	total := c.cartService.GetTotalCartValue(listCart)
	fmt.Println("list:", listCart)
	if err != nil {
		res := helpers.BuildErrorResponse("Fail fetch list products", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	res := helpers.BuildSuccessAddCart(true, "Ok", listCart, total)
	ctx.JSON(http.StatusOK, res)
}

// GetCartByID godoc
// @Security bearerAuth
// @Summary Get Cart By Id
// @Description Get cart by id
// @Tags Cart
// @Accept  json
// @Produce  json
// @Param  id path uint64 true "Cart ID"
// @Success 200 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 404 {object} helpers.Response
// @Router /cart/{id} [get]
func (c *cartController) FindById(ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		res := helpers.BuildErrorResponse("No param cart_id was found", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	helpers.BuildErrorResponse("Data not found", "No data with given cart_id", helpers.EmptyObj{})

	response, err := c.cartService.FindByID(ctx, id)

	if (response == &entity.Cart{}) {
		res := helpers.BuildErrorResponse("Data not found", "No data with given cart_id", helpers.EmptyObj{})
		ctx.JSON(http.StatusNotFound, res)
	} else {
		res := helpers.BuildResponse(true, "OK", response)
		ctx.JSON(http.StatusOK, res)
	}

}

// Add Cart godoc
// @Security bearerAuth
// @Summary Add Cart
// @Description Add Cart API
// @Tags Cart
// @Accept  json
// @Produce  json
// @Param product body request.RequestCreateCart true "Add cart"
// @Success 201 {object} helpers.Response
// @Failure 422 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /cart [post]
func (c *cartController) Create(ctx *gin.Context) {
	var reqCart request.RequestCreateCart
	err := ctx.ShouldBind(&reqCart)
	if err != nil {
		res := helpers.BuildErrorResponse("failed create add cart", err.Error(), helpers.EmptyObj{})
		ctx.JSON(http.StatusUnprocessableEntity, res)
		return
	} else {
		authHeader := ctx.GetHeader("Authorization")
		userID := c.getUserIDByToken(authHeader)
		convertedUserID, err := strconv.ParseUint(userID, 10, 64)
		if err == nil {
			reqCart.UserID = convertedUserID
		}
		fmt.Println("Product:", reqCart)
		result, err := c.cartService.Store(ctx, &reqCart)
		if err != nil {
			res := helpers.BuildErrorResponse("Failed to created cart", err.Error(), helpers.EmptyObj{})
			ctx.JSON(http.StatusBadRequest, res)
		}
		response := helpers.BuildResponse(true, "OK", result)
		ctx.JSON(http.StatusCreated, response)
	}
}

// Update Cart godoc
// @Security bearerAuth
// @Summary Update Cart
// @Description Update Cart API
// @Tags Cart
// @Accept  json
// @Produce  json
// @Param cart body request.RequestUpdateCart true "Update cart"
// @Success 200 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 404 {object} helpers.Response
// @Failure 403 {object} helpers.Response
// @Router /cart/{id} [put]
func (c *cartController) Update(ctx *gin.Context) {
	var cart request.RequestUpdateCart
	err := ctx.BindJSON(&cart)
	if err != nil {
		res := helpers.BuildErrorResponse("Not data cart", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	authHeader := ctx.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if err != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	if c.cartService.IsAllowedToEdit(ctx, userID, cart.ID) {

		id, errID := strconv.ParseUint(userID, 10, 64)
		if errID == nil {
			cart.UserID = id
		}

		result, err := c.cartService.UpdateCart(ctx, &cart)
		if err != nil {
			res := helpers.BuildErrorResponse("Failed updated cart", err.Error(), helpers.EmptyObj{})
			ctx.AbortWithStatusJSON(http.StatusNotFound, res)
			return
		}
		response := helpers.BuildResponse(true, "OK", result)
		ctx.JSON(http.StatusOK, response)
	} else {
		response := helpers.BuildErrorResponse("You dont have permission", "You are not the owner", helpers.EmptyObj{})
		ctx.JSON(http.StatusForbidden, response)
	}

}

// Delete Cart godoc
// @Security bearerAuth
// @Summary Remove cart
// @Description Delete a cart
// @Security bearerAuth
// @Tags Cart
// @Accept  json
// @Produce  json
// @Param  id path int true "Cart ID"
// @Success 200 {object} helpers.Response
// @Failure 404 {object} helpers.Response
// @Failure 400 {object} helpers.Response
// @Failure 403 {object} helpers.Response
// @Router /cart/{id} [delete]
func (c *cartController) Delete(ctx *gin.Context) {
	var cart entity.Cart
	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		res := helpers.BuildErrorResponse("No param cart_id was found", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusNotFound, res)
		return
	}
	cart.ID = id
	authHeader := ctx.GetHeader("Authorization")
	token, errToken := c.jwtService.ValidateToken(authHeader)
	if err != nil {
		panic(errToken.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	if c.cartService.IsAllowedToEdit(ctx, userID, cart.ID) {
		err = c.cartService.Delete(ctx, &cart)
		if err != nil {
			result := helpers.BuildErrorResponse("Failed deleted cart cek your permissions", "Failed deleted", helpers.EmptyObj{})
			ctx.JSON(http.StatusBadRequest, result)
		}
		res := helpers.BuildResponse(true, "Deleted", helpers.EmptyObj{})
		ctx.JSON(http.StatusOK, res)
	} else {
		response := helpers.BuildErrorResponse("You dont have permission", "You are not the owner", helpers.EmptyObj{})
		ctx.JSON(http.StatusForbidden, response)
	}
}

// Get Cart Search, Lazy load pagination godoc
// @Security bearerAuth
// @Summary List existing cart by search,sort and pagination
// @Description Get all the existing cart by search, sort and pagination
// @Tags Cart
// @Accept  json
// @Produce  json
// @param page query string false "cart search by page" Format(name)
// @param limit query string false "cart search by limit" Format(name)
// @param sort query string false "cart search by sort" Format(name)
// @param name.constains query string false "cart search by name" Format(name)
// @param name.in query string false "cart search by name" Format(name)
// @param name.equal query string false "cart search by name" Format(name)
// @Success 200 {array} helpers.Response
// @Failure 400 {object} helpers.Response
// @Router /carts/search [get]
func (c *cartController) PaginationCart(ctx *gin.Context) {
	code := http.StatusOK
	pagination := helpers.GeneratePaginationRequest(ctx)

	response, err := c.cartService.PaginantionCart(ctx, pagination)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed pagination products", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	if !response.Status {
		code = http.StatusBadRequest
	}

	res := helpers.BuildResponse(true, "Ok", response)
	ctx.AbortWithStatusJSON(code, res)
}

func (c *cartController) getUserIDByToken(token string) string {
	Token, err := c.jwtService.ValidateToken(token)
	if err != nil {
		panic(err.Error())
	}
	claims := Token.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	return id
}
